import * as functions from 'firebase-functions';
import { getNotSupportedError, getParamMissingError } from './errors';

/**
 * Check if all requried params are found in a request. If not an error response is sent describing the issue.
 * 
 * @param req HTTP / HTTPS request object.
 * @param res Function response object.
 * @param params Array of param string to check for. If one isn't found a HTTP 500 error is sent.
 * @param next Function to call if required params are found.
 */
export const requireParams = (
    req: functions.https.Request | functions.Request,
    res: functions.Response,
    params: string[],
    next: any) => {
    for (const param of params) {
        if (param && !req.query[param]) {
            return res.status(500).send(getParamMissingError(param));
        }
    }
    return next();
}

/**
 * Check if all requried params are found in a request. If not an error response is sent describing the issue.
 * 
 * @param req HTTP / HTTPS request object.
 * @param res Function response object.
 * @param method String method expected for this request.
 * @param next Function to call if required params are found.
 */
export const requireMethod = (req: functions.https.Request | functions.Request,
    res: functions.Response,
    method: string,
    next: any) => {
    const meth = method.toLocaleUpperCase();
    if (req.method.toLocaleUpperCase() !== meth) {
        return res.status(501).send(getNotSupportedError(meth));
    }

    return next();
}