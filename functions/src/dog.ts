export interface Doggo {

    id?: number;
    name?: string;
    age?: number;
    owner?: {
        name: string;
        location: string;
    };
    ranking?: number;
    active?: boolean;
};


export const defaultDoggo: Doggo = {
    id: 0,
    name: 'Default doggo',
    age: 0,
    active: true
};