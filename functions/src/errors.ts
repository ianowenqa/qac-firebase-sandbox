

export const getNotSupportedError = (requiredMethod: string) => ({
    message: `Operation not supported. Use ${requiredMethod ? requiredMethod.toUpperCase() : 'correct'} method.`
});

export const getParamMissingError = (requiredParam: string) => ({
    message: `Request failed. Request must include '${requiredParam}' param!`
});