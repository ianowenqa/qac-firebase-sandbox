import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { getNotSupportedError } from './errors';
import { User } from './user';

import { FirebaseFunctionsRateLimiter } from 'firebase-functions-rate-limiter';
import * as cors from 'cors';
import { requireMethod, requireParams } from './handlers';


const corsHandler = cors({ origin: true });
admin.initializeApp(functions.config().firebase);

const limiter = FirebaseFunctionsRateLimiter.withRealtimeDbBackend(
    {
        name: "rate_limiter_collection",
        maxCalls: 15,
        periodSeconds: 15,
    },
    admin.database()
);


export const getUsers = functions.https.onRequest(async (req, res) => {
    await limiter.rejectOnQuotaExceededOrRecordUsage();
    res.set('Access-Control-Allow-Origin', '*');
    if (req.method !== 'GET') {
        return res.status(501).send(getNotSupportedError('GET'));
    }

    return admin.database().ref('users/').once('value').then(snapshot => {
        return res.status(200).send(snapshot.val());
    });

});

export const setUser = functions.https.onRequest((req, res) => {
    // res.set('Access-Control-Allow-Origin', '*');
    // res.set('Accept', 'application/json');

    // if (req.method !== 'POST') {
    //     if(req.method === 'OPTIONS') {
    //         return res.status(200).send();
    //     }
    //     return res.status(501).send(getNotSupportedError('POST'));
    // }

    const { username, ...user } = <User>req.body;

    return corsHandler(req, res, async () => {
        await limiter.rejectOnQuotaExceededOrRecordUsage();

        if (!username) {
            return res.status(500).send({ message: 'Username must be defined!' });
        }
        if (!user.password) {
            return res.status(500).send({ message: 'Password must be defined!' });
        }
        if (username && username.toLowerCase() === 'admin') {
            return res.status(500).send({ message: 'Don\'t change admin!' });
        }

        return admin.database().ref(`users/${username}`).once('value')
            .then(snapshot => {
                // TODO: Check if exists before setting
                return admin.database().ref(`users/${username}`).set(user);
            }).then(() => {
                return res.status(201).send(user);
            })
            .catch(err => {
                return res.status(500).send(err);
            });
    });

});


export const deleteUser = functions.https.onRequest((req, res) => {

    return corsHandler(req, res, async () => {
        await limiter.rejectOnQuotaExceededOrRecordUsage();
        if (req.method !== 'DELETE') {
            return res.status(501).send(getNotSupportedError('DELETE'));
        }

        if (!req.query['username']) {
            return res.status(500).send({ message: 'Request must include \'username\' param!' });
        }

        if (req.query['username'].toLowerCase() === 'admin') {
            return res.status(500).send({ message: 'Don\'t delete admin!' });
        }
        return admin.database().ref(`users/${req.query['username']}`).remove().then((data) => {
            return res.status(200).send(data);
        });
    });

});

export const login = functions.https.onRequest(
    (req, res) => corsHandler(req, res,
        () => requireMethod(req, res, 'GET',
            () => requireParams(req, res, ['username', 'password'],
                async () => {
                    await limiter.rejectOnQuotaExceededOrRecordUsage();
                    return admin.database().ref(`users/${req.query['username']}`).once('value').then(
                        (snapshot) => {
                            if (snapshot.val() && snapshot.val().password === req.query['password']) {
                                return res.status(200).send(snapshot.val());
                            } else {
                                return res.status(401).send({ message: 'Login failed.' });
                            }
                        }
                    )
                }
            )
        )
    )
);

export const takeMeHome = functions.https.onRequest(async (_req, res) => {
    await limiter.rejectOnQuotaExceededOrRecordUsage();
    res.set('Access-Control-Allow-Origin', '*');
    res.send(`
Almost heaven, West Virginia
Blue Ridge Mountains, Shenandoah River
Life is old there, older than the trees
Younger than the mountains, blowing like a breeze
Country roads, take me home
To the place I belong
West Virginia, mountain mama
Take me home, country roads
All my memories gather round her
Miner's lady, stranger to blue water
Dark and dusty, painted on the sky
Misty taste of moonshine, teardrop in my eye
Country roads, take me home
To the place I belong
West Virginia, mountain mama
Take me home, country roads
I hear her voice, in the morning hour she calls me
The radio reminds me of my home far away
And driving down the road I get a feeling
That I should have been home yesterday, yesterday
Country roads, take me home
To the place I belong
West Virginia, mountain mama
Take me home, country roads
Country roads, take me home
To the place I belong
West Virginia, mountain mama
Take me home, country roads
Take me home, down country roads
Take me home, down country roads
    `);
});
